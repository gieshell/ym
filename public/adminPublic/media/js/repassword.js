//声明全局变量 检测input 是否输入正确
	var USER = false;
	var PASS = false;
	var REPASS = false;
	var EMAIL = false;

	//绑定表单提交事件
	$('form').submit(function(){
		//触发所有的丧失焦点事件
		$('input').trigger('blur');
		if(USER && PASS && REPASS && EMAIL){
			return true;
		}

		//阻止默认行为
		return false;
	})

	//用户名丧失焦点事件
	$('input[name=username]').blur(function(){
		//检测用户名是否正确
		var reg = /^\w{5,18}$/;
		//获取用户名
		var username = $(this).val();
		if(reg.test(username)){
			$(this).next().children().attr('class','icon-ok');
			$(this).next().attr('class','input-success tooltips');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group success');
		}else{
			//错误
			$(this).next().children().attr('class','icon-exclamation-sign');
			$(this).next().attr('class','input-error error');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group error');
			return false;
		}
		var inp = $(this);
		//get发送
		$.get('/admin/user/select',{username:username},function(data){
			if(data == 2){
				inp.next().children().attr('class','icon-ok');
				inp.next().attr('class','input-success tooltips');
				inp.parent('div').addClass('controls input-icon');
				inp.parents('.control-group').attr('class','control-group success');
				$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('可以使用该用户名');
				$('#jdt').css('width','25%');
				$('.jd').addClass('active');
				//修改全局变量
				USER = true;
			}else{
				inp.next().children().attr('class','icon-exclamation-sign');
				inp.next().attr('class','input-error error');
				inp.parent('div').addClass('controls input-icon');
				inp.parents('.control-group').attr('class','control-group error');
				$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('该用户名已经存在');

				return false;
			}
		//第四个参数 如果要加参数,返回的格式就必须是json格式
		},'json')
	})

	$('input[name=password]').blur(function(){
		//获取密码的值
		var pass = $(this).val();
		//正则
		var reg = /^\w{5,18}$/;
		if(reg.test(pass)){
			//正确
			$(this).next().children().attr('class','icon-ok');
			$(this).next().attr('class','input-success tooltips');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group success');
			$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('密码安全度高');
			$('#jdt').css('width','50%');
			$('.jd1').addClass('active');
			PASS = true;
		}else{
			//错误
			$(this).next().children().attr('class','icon-exclamation-sign');
			$(this).next().attr('class','input-error error');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group error');
			$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('密码请满足5,18位数组字母下划线');
			return false;
		}
	})

	//确认密码
	$('input[name=repassword]').blur(function(){
		//获取确认密码的值
		var repass = $(this).val();
		//获取密码的值
		var pass = $('input[name=password]').val();
		if(repass == ''){
			//错误
			$(this).next().children().attr('class','icon-exclamation-sign');
			$(this).next().attr('class','input-error error');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group error');
			$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('重置密码不允许为空');
			return false;
		}
		//判断
		if(repass == pass){
			//正确
			$(this).next().children().attr('class','icon-ok');
			$(this).next().attr('class','input-success tooltips');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group success');
			$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('密码安全度高');
			$('#jdt').css('width','75%');
			$('.jd2').addClass('active');
			REPASS = true;
		}else{
			//错误
			$(this).next().children().attr('class','icon-exclamation-sign');
			$(this).next().attr('class','input-error error');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group error');
			$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('两次密码输入不一致');
			return false;
		}
	})

	$('input[name=email]').blur(function(){
		//获取邮箱的值
		var email = $(this).val();
		//正则
		var reg = /^\w+@\w+\.(com|cn|com.cn|net|org)$/;
		//检测
		if(reg.test(email)){
			//正确
			$(this).next().children().attr('class','icon-ok');
			$(this).next().attr('class','input-success tooltips');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group success');
			$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('邮箱格式正确');
			$('#jdt').css('width','100%');
			$('.jd3').addClass('active');
			EMAIL = true;
		}else{
			$(this).next().children().attr('class','icon-exclamation-sign');
			$(this).next().attr('class','input-error error');
			$(this).parent('div').addClass('controls input-icon');
			$(this).parents('.control-group').attr('class','control-group error');
			$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('邮箱不合法');
			return false;
		}
	})