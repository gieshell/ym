@extends('layout.user')
@section('title', '后台首页')
@section('content')
<div class="dashboard">
      	<!-- success代表成功 -->
      	@if(session('success'))
      	<div class="alert alert-success">
      		
      		<button class="close" data-dismiss="alert"></button>

      		<strong>{{session('success')}}</strong>

      	</div>
      	@endif
      	@if (count($errors) > 0)
	        <div class="alert alert-error">
	            <ul>
	                @foreach ($errors->all() as $error)
	                    <li>{{ $error }}</li>
	                @endforeach
	            </ul>
	        </div>
   		 @endif
</div>
@endsection