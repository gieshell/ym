@extends('layout.user')
@section('title', '后台用户列表')
@section('content')
<div class="tabbable tabbable-custom tabbable-full-width">

	<ul class="nav nav-tabs">

		<li class="active"><a data-toggle="tab" href="#tab_1_2">我的资料</a></li>

		<li class=""><a data-toggle="tab" href="#tab_1_3">修改我的资料</a></li>

	</ul>
	<!-- success代表成功 -->
	@if(session('success'))
	<div class="alert alert-success">
		
		<button class="close" data-dismiss="alert"></button>

		<strong>{{session('success')}}</strong>

	</div>
	@endif
	@if (count($errors) > 0)
	    <div class="alert alert-error">
	  	  <button class="close" data-dismiss="alert"></button>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<div class="tab-content">

		<div id="tab_1_1" class="tab-pane row-fluid">

			<ul class="unstyled profile-nav span3">

				<li><img alt="" src="/adminPublic/media/image/profile-img.png"> <a class="profile-edit" href="#">我的个性头像</a></li>

				<li><a href="#">Projects</a></li>

				<li><a href="#">Messages <span>3</span></a></li>

				<li><a href="#">Friends</a></li>

				<li><a href="#">Settings</a></li>

			</ul>

			<div class="span9">

				<div class="row-fluid">

					<div class="span8 profile-info">

						<h1>John Doe</h1>

						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat volutpat.</p>

						<p><a href="#">www.mywebsite.com</a></p>

						<ul class="unstyled inline">

							<li><i class="icon-map-marker"></i> Spain</li>

							<li><i class="icon-calendar"></i> 18 Jan 1982</li>

							<li><i class="icon-briefcase"></i> Design</li>

							<li><i class="icon-star"></i> Top Seller</li>

							<li><i class="icon-heart"></i> BASE Jumping</li>

						</ul>

					</div>

					<!--end span8-->

					<div class="span4">

						<div class="portlet sale-summary">

							<div class="portlet-title">

								<div class="caption">Sales Summary</div>

								<div class="tools">

									<a href="javascript:;" class="reload"></a>

								</div>

							</div>

							<ul class="unstyled">

								<li>

									<span class="sale-info">TODAY SOLD <i class="icon-img-up"></i></span> 

									<span class="sale-num">23</span>

								</li>

								<li>

									<span class="sale-info">WEEKLY SALES <i class="icon-img-down"></i></span> 

									<span class="sale-num">87</span>

								</li>

								<li>

									<span class="sale-info">TOTAL SOLD</span> 

									<span class="sale-num">2377</span>

								</li>

								<li>

									<span class="sale-info">EARNS</span> 

									<span class="sale-num">$37.990</span>

								</li>

							</ul>

						</div>

					</div>

					<!--end span4-->

				</div>

				<!--end row-fluid-->

				<div class="tabbable tabbable-custom tabbable-custom-profile">

					<ul class="nav nav-tabs">

						<li class="active"><a data-toggle="tab" href="#tab_1_11">Latest Customers</a></li>

						<li class=""><a data-toggle="tab" href="#tab_1_22">Feeds</a></li>

					</ul>

					<div class="tab-content">

						<div id="tab_1_11" class="tab-pane active">

							<div style="display: block;" class="portlet-body">

								<table class="table table-striped table-bordered table-advance table-hover">

									<thead>

										<tr>

											<th><i class="icon-briefcase"></i> Company</th>

											<th class="hidden-phone"><i class="icon-question-sign"></i> Descrition</th>

											<th><i class="icon-bookmark"></i> Amount</th>

											<th></th>

										</tr>

									</thead>

									<tbody>

										<tr>

											<td><a href="#">Pixel Ltd</a></td>

											<td class="hidden-phone">Server hardware purchase</td>

											<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>

											<td><a href="#" class="btn mini green-stripe">View</a></td>

										</tr>

										<tr>

											<td>

												<a href="#">

												Smart House

												</a>  

											</td>

											<td class="hidden-phone">Office furniture purchase</td>

											<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>

											<td><a href="#" class="btn mini blue-stripe">View</a></td>

										</tr>

										<tr>

											<td>

												<a href="#">

												FoodMaster Ltd

												</a>

											</td>

											<td class="hidden-phone">Company Anual Dinner Catering</td>

											<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>

											<td><a href="#" class="btn mini blue-stripe">View</a></td>

										</tr>

										<tr>

											<td>

												<a href="#">

												WaterPure Ltd

												</a>

											</td>

											<td class="hidden-phone">Payment for Jan 2013</td>

											<td>610.50$ <span class="label label-danger label-mini">Overdue</span></td>

											<td><a href="#" class="btn mini red-stripe">View</a></td>

										</tr>

										<tr>

											<td><a href="#">Pixel Ltd</a></td>

											<td class="hidden-phone">Server hardware purchase</td>

											<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>

											<td><a href="#" class="btn mini green-stripe">View</a></td>

										</tr>

										<tr>

											<td>

												<a href="#">

												Smart House

												</a>  

											</td>

											<td class="hidden-phone">Office furniture purchase</td>

											<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>

											<td><a href="#" class="btn mini blue-stripe">View</a></td>

										</tr>

										<tr>

											<td>

												<a href="#">

												FoodMaster Ltd

												</a>

											</td>

											<td class="hidden-phone">Company Anual Dinner Catering</td>

											<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>

											<td><a href="#" class="btn mini blue-stripe">View</a></td>

										</tr>

									</tbody>

								</table>

							</div>

						</div>

						<!--tab-pane-->

						<div id="tab_1_22" class="tab-pane">

							<div id="tab_1_1_1" class="tab-pane active">

								<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;"><div data-rail-visible1="1" data-always-visible="1" data-height="290px" class="scroller" style="overflow: hidden; width: auto; height: 290px;">

									<ul class="feeds">

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-success">                        

															<i class="icon-bell"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															You have 4 pending tasks.

															<span class="label label-important label-mini">

															Take action 

															<i class="icon-share-alt"></i>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													Just now

												</div>

											</div>

										</li>

										<li>

											<a href="#">

												<div class="col1">

													<div class="cont">

														<div class="cont-col1">

															<div class="label label-success">                        

																<i class="icon-bell"></i>

															</div>

														</div>

														<div class="cont-col2">

															<div class="desc">

																New version v1.4 just lunched!   

															</div>

														</div>

													</div>

												</div>

												<div class="col2">

													<div class="date">

														20 mins

													</div>

												</div>

											</a>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-important">                      

															<i class="icon-bolt"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															Database server #12 overloaded. Please fix the issue.                      

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													24 mins

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													30 mins

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-success">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													40 mins

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-warning">                        

															<i class="icon-plus"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New user registered.                

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													1.5 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-success">                        

															<i class="icon-bell-alt"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															Web server hardware needs to be upgraded. 

															<span class="label label-inverse label-mini">Overdue</span>             

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													2 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label">                       

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													3 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-warning">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													5 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													18 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label">                       

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													21 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													22 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label">                       

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													21 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													22 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label">                       

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													21 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													22 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label">                       

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													21 hours

												</div>

											</div>

										</li>

										<li>

											<div class="col1">

												<div class="cont">

													<div class="cont-col1">

														<div class="label label-info">                        

															<i class="icon-bullhorn"></i>

														</div>

													</div>

													<div class="cont-col2">

														<div class="desc">

															New order received. Please take care of it.                 

														</div>

													</div>

												</div>

											</div>

											<div class="col2">

												<div class="date">

													22 hours

												</div>

											</div>

										</li>

									</ul>

								</div><div class="slimScrollBar ui-draggable" style="background: rgb(161, 178, 189) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>

							</div>

						</div>

						<!--tab-pane-->

					</div>

				</div>

			</div>

			<!--end span9-->

		</div>

		<!--end tab-pane-->

		<div id="tab_1_2" class="tab-pane profile-classic row-fluid active">

			<div class="span2"><img alt="" id="tx" src="@if($data->pic)
												  {{$data->pic}}
												@else
												  /adminPublic/media/image/profile-img.png
												@endif
			"> <a class="profile-edit" href="#">用户头像</a></div>

			<ul class="unstyled span10">

				<li><span>我的名字:</span>{{$data->username}}</li>

				<li><span>年龄:</span>@if($data->age)
												{{$data->age}}
										   @else
												您的年龄还未填写
										   @endif
				</li>
				<li><span>性别:</span>@if($data->sex == 0)
												男
										   @else
												女
										   @endif</li>

				<li><span>出生日期:</span>@if($data->birthday)
												{{$data->birthday}}
										   @else
												您的出生日期还未填写
										   @endif</li>
				<li><span>邮箱:</span> {{$data->email}}</li>
				<li><span>手机号:</span>@if($data->phone)
												{{$data->phone}}
										   @else
												您的手机号还未填写
										   @endif</li>

				<li><span>注册时间:</span>{{date('Y-m-d',$data->regtime)}}</li>

				<li><span>我已经来驭梦:</span>{{$newtime}}天</li>

			</ul>

		</div>

		<!--tab_1_2-->

		<div id="tab_1_3" class="tab-pane row-fluid profile-account">

			<div class="row-fluid">

				<div class="span12">

						<!-- 头像开始-->
					<div class="span2">
						<form id='myupload' action="/user/info/imgupload" method="post" enctype="multipart/form-data">
							{{ csrf_field() }}
							<img  id="file" onclick="uploadphoto.click()" src="@if($data->pic)
												{{$data->pic}}
											 @else
											 /adminPublic/media/image/profile-img.png
											 @endif
							">
							<input type="file" id="uploadphoto" name="file" style="display:none" >
							<input type="hidden"  name="id"  value="{{$data->id}}" id="btn_file" style="display:none" >
							<br>
						</form>						
					</div>
						<!-- 头像结束-->

					<div class="span9">

						<div class="tab-content">

							<div class="tab-pane active" id="tab_1-1">

								<div class="accordion in collapse" id="accordion1-1" style="height: auto;">

									<form action="/user/info/update" method="post">
										{{ csrf_field() }}
										<label class="control-label">用户名</label>

										<input type="text" class="m-wrap span8" disabled=""  value="{{$data->username}}">
										<input type="hidden" name="id" value="{{$data->id}}">
										
										<label class="control-label">性别</label>
										<div class="controls">
											<label class="radio">

											<div class="radio"><input type="radio"   @if($data->sex == 0) 
																						checked
																					@endif
											 name="sex" value="0"></div>

											男

											</label>

											<label class="radio">

											<div class="radio"><input type="radio" @if($data->sex == 1) 
																						checked
																					@endif
											name="sex" value="1" ></div>

											女

											</label>  

										</div>
										
										<label class="control-label" >年龄</label>

										<input type="text" class="m-wrap span8" name="age" value="{{$data->age}}">

										<label class="control-label">出生日期</label>
										
										<input type="text" class="m-wrap span8"   name="birthday" value="{{$data->birthday}}">

										<label class="control-label">邮箱</label>

										<input type="text" class="m-wrap span8" name="email" value="{{$data->email}}">

										<label class="control-label">手机号</label>

										<input type="text" class="m-wrap span8" name="phone" value="{{$data->phone}}">

										<div class="submit-btn">

											<button class="btn green">确认修改<i class="m-icon-swapright m-icon-white"></i></button>
										</div>

									</form>

								</div>

							</div>

							<div class="tab-pane" id="tab_2-2">

								<div class="accordion collapse in" id="accordion2-2" style="height: auto;">

									<form action="#">

										<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>

										<br>

										<div class="controls">

											<div style="width: 291px; height: 170px;" class="thumbnail">

												<!-- <img alt="" src="/adminPublic/media/image/AAAAAA&amp;text=no+image"> -->

											</div>

										</div>

										<div class="space10"></div>

										<div data-provides="fileupload" class="fileupload fileupload-new">

											<div class="input-append">

												<div class="uneditable-input">

													<i class="icon-file fileupload-exists"></i> 

													<span class="fileupload-preview"></span>

												</div>

												<span class="btn btn-file">

												<span class="fileupload-new">Select file</span>

												<span class="fileupload-exists">Change</span>

												<input type="file" class="default">

												</span>

												<a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>

											</div>

										</div>

										<div class="clearfix"></div>

										<div class="controls">

											<span class="label label-important">NOTE!</span>

											<span>You can write some information here..</span>

										</div>

										<div class="space10"></div>

										<div class="submit-btn">

											<a class="btn green" href="#">Submit</a>

											<a class="btn" href="#">Cancel</a>

										</div>

									</form>

								</div>

							</div>

							<div class="tab-pane" id="tab_3-3">

								<div class="accordion collapse in" id="accordion3-3" style="height: auto;">

									<form action="#">

										<label class="control-label">Current Password</label>

										<input type="password" class="m-wrap span8">

										<label class="control-label">New Password</label>

										<input type="password" class="m-wrap span8">

										<label class="control-label">Re-type New Password</label>

										<input type="password" class="m-wrap span8">

										<div class="submit-btn">

											<a class="btn green" href="#">Change Password</a>

											<a class="btn" href="#">Cancel</a>

										</div>

									</form>

								</div>

							</div>

							<div class="tab-pane" id="tab_4-4">

								<div class="accordion collapse in" id="accordion4-4" style="height: auto;">

									<form action="#">

										<div class="profile-settings row-fluid">

											<div class="span9">

												<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..</p>

											</div>

											<div class="control-group span3">

												<div class="controls">

													<label class="radio">

													<div class="radio"><span><input type="radio" value="option1" name="optionsRadios1"></span></div>

													Yes

													</label>

													<label class="radio">

													<div class="radio"><span class="checked"><input type="radio" checked="" value="option2" name="optionsRadios1"></span></div>

													No

													</label>  

												</div>

											</div>

										</div>

										<!--end profile-settings-->

										<div class="profile-settings row-fluid">

											<div class="span9">

												<p>Enim eiusmod high life accusamus terry richardson ad squid wolf moon</p>

											</div>

											<div class="control-group span3">

												<div class="controls">

													<label class="checkbox">

													<div class="checker"><span><input type="checkbox" value=""></span></div> All

													</label>

													<label class="checkbox">

													<div class="checker"><span><input type="checkbox" value=""></span></div> Friends

													</label>

												</div>

											</div>

										</div>

										<!--end profile-settings-->

										<div class="profile-settings row-fluid">

											<div class="span9">

												<p>Pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson</p>

											</div>

											<div class="control-group span3">

												<div class="controls">

													<label class="checkbox">

													<div class="checker"><span><input type="checkbox" value=""></span></div> Yes

													</label>

												</div>

											</div>

										</div>

										<!--end profile-settings-->

										<div class="profile-settings row-fluid">

											<div class="span9">

												<p>Cliche reprehenderit enim eiusmod high life accusamus terry</p>

											</div>

											<div class="control-group span3">

												<div class="controls">

													<label class="checkbox">

													<div class="checker"><span><input type="checkbox" value=""></span></div> Yes

													</label>

												</div>

											</div>

										</div>

										<!--end profile-settings-->

										<div class="profile-settings row-fluid">

											<div class="span9">

												<p>Oiusmod high life accusamus terry richardson ad squid wolf fwopo</p>

											</div>

											<div class="control-group span3">

												<div class="controls">

													<label class="checkbox">

													<div class="checker"><span><input type="checkbox" value=""></span></div> Yes

													</label>

												</div>

											</div>

										</div>

										<!--end profile-settings-->

										<div class="space5"></div>

										<div class="submit-btn">

											<a class="btn green" href="#">Save Changes</a>

											<a class="btn" href="#">Cancel</a>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

					<!--end span9-->                                   

				</div>

			</div>

		</div>

		<!--end tab-pane-->

		<div id="tab_1_4" class="tab-pane">

			<div class="row-fluid add-portfolio">

				<div class="pull-left">

					<span>502 Items sold this week</span>

				</div>

				<div class="pull-left">

					<a class="btn icn-only green" href="#">Add a new Project <i class="m-icon-swapright m-icon-white"></i></a>                          

				</div>

			</div>

			<!--end add-portfolio-->

			<div class="row-fluid portfolio-block">

				<div class="span5 portfolio-text">

					<img alt="" src="/adminPublic/media/image/logo_metronic.jpg">

					<div class="portfolio-text-info">

						<h4>Metronic - Responsive Template</h4>

						<p>Lorem ipsum dolor sit consectetuer adipiscing elit.</p>

					</div>

				</div>

				<div style="overflow:hidden;" class="span5">

					<div class="portfolio-info">

						Today Sold

						<span>187</span>

					</div>

					<div class="portfolio-info">

						Total Sold

						<span>1789</span>

					</div>

					<div class="portfolio-info">

						Earns

						<span>$37.240</span>

					</div>

				</div>

				<div class="span2 portfolio-btn">

					<a class="btn bigicn-only" href="#"><span>Manage</span></a>                      

				</div>

			</div>

			<!--end row-fluid-->

			<div class="row-fluid portfolio-block">

				<div class="span5 portfolio-text">

					<img alt="" src="/adminPublic/media/image/logo_azteca.jpg">

					<div class="portfolio-text-info">

						<h4>Metronic - Responsive Template</h4>

						<p>Lorem ipsum dolor sit consectetuer adipiscing elit.</p>

					</div>

				</div>

				<div class="span5">

					<div class="portfolio-info">

						Today Sold

						<span>24</span>

					</div>

					<div class="portfolio-info">

						Total Sold

						<span>660</span>

					</div>

					<div class="portfolio-info">

						Earns

						<span>$7.060</span>

					</div>

				</div>

				<div class="span2 portfolio-btn">

					<a class="btn bigicn-only" href="#"><span>Manage</span></a>                      

				</div>

			</div>

			<!--end row-fluid-->

			<div class="row-fluid portfolio-block">

				<div class="span5 portfolio-text">

					<img alt="" src="/adminPublic/media/image/logo_conquer.jpg">

					<div class="portfolio-text-info">

						<h4>Metronic - Responsive Template</h4>

						<p>Lorem ipsum dolor sit consectetuer adipiscing elit.</p>

					</div>

				</div>

				<div style="overflow:hidden;" class="span5">

					<div class="portfolio-info">

						Today Sold

						<span>24</span>

					</div>

					<div class="portfolio-info">

						Total Sold

						<span>975</span>

					</div>

					<div class="portfolio-info">

						Earns

						<span>$21.700</span>

					</div>

				</div>

				<div class="span2 portfolio-btn">

					<a class="btn bigicn-only" href="#"><span>Manage</span></a>                      

				</div>

			</div>

			<!--end row-fluid-->

		</div>

		<!--end tab-pane-->

		<div id="tab_1_6" class="tab-pane row-fluid">

			<div class="row-fluid">

				<div class="span12">

					<div class="span3">

						<ul class="ver-inline-menu tabbable margin-bottom-10">

							<li class="active">

								asdsadas                              

							</li>

							<li><a href="#tab_2" data-toggle="tab"><i class="icon-group"></i> Membership</a></li>

							<li><a href="#tab_3" data-toggle="tab"><i class="icon-leaf"></i> Terms Of Service</a></li>

							<li><a href="#tab_1" data-toggle="tab"><i class="icon-info-sign"></i> License Terms</a></li>

							<li><a href="#tab_2" data-toggle="tab"><i class="icon-tint"></i> Payment Rules</a></li>

							<li><a href="#tab_3" data-toggle="tab"><i class="icon-plus"></i> Other Questions</a></li>

						</ul>

					</div>

					<div class="span9">

						<div class="tab-content">

							<div class="tab-pane active" id="tab_1">

								<div class="accordion collapse in" id="accordion1" style="height: auto;">

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">

											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?

											</a>

										</div>

										<div id="collapse_1" class="accordion-body collapse in">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2">

											Pariatur cliche reprehenderit enim eiusmod highr brunch ?

											</a>

										</div>

										<div id="collapse_2" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3">

											Food truck quinoa nesciunt laborum eiusmod nim eiusmod high life accusamus  ?

											</a>

										</div>

										<div id="collapse_3" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">

											High life accusamus terry richardson ad ?

											</a>

										</div>

										<div id="collapse_4" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_5">

											Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_5" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_6">

											Wolf moon officia aute non cupidatat skateboard dolor brunch ?

											</a>

										</div>

										<div id="collapse_6" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="tab-pane" id="tab_2">

								<div class="accordion collapse in" id="accordion2" style="height: auto;">

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">

											Cliche reprehenderit, enim eiusmod high life accusamus enim eiusmod ?

											</a>

										</div>

										<div id="collapse_2_1" class="accordion-body collapse in">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2">

											Pariatur cliche reprehenderit enim eiusmod high life non cupidatat skateboard dolor brunch ?

											</a>

										</div>

										<div id="collapse_2_2" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3">

											Food truck quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_2_3" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_4">

											High life accusamus terry richardson ad squid enim eiusmod high ?

											</a>

										</div>

										<div id="collapse_2_4" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_5">

											Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_2_5" class="accordion-body collapse">

											<div class="accordion-inner">

												<p>

													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

												</p>

												<p> 

													moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor

												</p>

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_6">

											Wolf moon officia aute non cupidatat skateboard dolor brunch ?

											</a>

										</div>

										<div id="collapse_2_6" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_7">

											Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_2_7" class="accordion-body collapse">

											<div class="accordion-inner">

												<p>

													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

												</p>

												<p> 

													moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor

												</p>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div class="tab-pane" id="tab_3">

								<div class="accordion collapse in" id="accordion3" style="height: auto;">

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">

											Cliche reprehenderit, enim eiusmod ?

											</a>

										</div>

										<div id="collapse_3_1" class="accordion-body collapse in">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">

											Pariatur skateboard dolor brunch ?

											</a>

										</div>

										<div id="collapse_3_2" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">

											Food truck quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_3_3" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4">

											High life accusamus terry richardson ad squid enim eiusmod high ?

											</a>

										</div>

										<div id="collapse_3_4" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5">

											Reprehenderit enim eiusmod high  eiusmod ?

											</a>

										</div>

										<div id="collapse_3_5" class="accordion-body collapse">

											<div class="accordion-inner">

												<p>

													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

												</p>

												<p> 

													moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor

												</p>

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6">

											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?

											</a>

										</div>

										<div id="collapse_3_6" class="accordion-body collapse">

											<div class="accordion-inner">

												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7">

											Reprehenderit enim eiusmod high life accusamus aborum eiusmod ?

											</a>

										</div>

										<div id="collapse_3_7" class="accordion-body collapse">

											<div class="accordion-inner">

												<p>

													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

												</p>

												<p> 

													moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor

												</p>

											</div>

										</div>

									</div>

									<div class="accordion-group">

										<div class="accordion-heading">

											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8">

											Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?

											</a>

										</div>

										<div id="collapse_3_8" class="accordion-body collapse">

											<div class="accordion-inner">

												<p>

													Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.

												</p>

												<p> 

													moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor

												</p>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<!--end span9-->                                   

				</div>

			</div>

		</div>

		<!--end tab-pane-->

	</div>

</div>

<script>
	$(document).ready(function(e) {
	   $("#uploadphoto").change(function(){
	  	 $("#myupload").ajaxSubmit({ 
	  		dataType:  'json', //数据格式为json 
	  		success: function(data) {
				if(data){ 
					$('#file').attr('src',data);
					$('#tx').attr('src',data);
				}else{
					alert('上传失败');
				}	
	  		}, 
	  		error:function(){ //上传失败 
	  		   alert("上传失败"); 
	  		} 
	  	}); 
	   });

	});
</script>
@endsection