@extends('layout.index')
@section('content')
<div class="row-fluid">

	<div class="span12">

		<div class="portlet box blue" id="form_wizard_1">

			<div class="portlet-title">

				<div class="caption">

					<i class="icon-reorder"></i> Form Wizard - <span class="step-title">Step 1 of 4</span>

				</div>

				<div class="tools hidden-phone">

					<a href="javascript:;" class="collapse"></a>

					<a href="#portlet-config" data-toggle="modal" class="config"></a>

					<a href="javascript:;" class="reload"></a>

					<a href="javascript:;" class="remove"></a>

				</div>

			</div>

			<div style="display: block;" class="portlet-body form">

				<form novalidate="novalidate" action="/admin/attribute/insert" class="form-horizontal" id="submit_form" method='post'>

					<div class="form-wizard">

						<div class="navbar steps">

							<div class="navbar-inner">

								<ul class="row-fluid nav nav-pills">

									<li class="span3 active">

										<a href="#tab1" data-toggle="tab" class="step active">

										<span class="number">1</span>

										<span class="desc"><i class="icon-ok"></i> Account Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab2" data-toggle="tab" class="step">

										<span class="number">2</span>

										<span class="desc"><i class="icon-ok"></i> Profile Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab3" data-toggle="tab" class="step">

										<span class="number">3</span>

										<span class="desc"><i class="icon-ok"></i> Billing Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab4" data-toggle="tab" class="step">

										<span class="number">4</span>

										<span class="desc"><i class="icon-ok"></i> Confirm</span>   

										</a> 

									</li>

								</ul>

							</div>

						</div>

						<div id="bar" class="progress progress-success progress-striped">

							<div style="width: 25%;" class="bar"></div>

						</div>

						<div class="tab-content">

							<div class="alert alert-error hide">

								<button class="close" data-dismiss="alert"></button>

								You have some form errors. Please check below.

							</div>

							<div class="alert alert-success hide">

								<button class="close" data-dismiss="alert"></button>

								Your form validation is successful!

							</div>

							<div class="tab-pane active" id="tab1">

								<h3 class="block">属性分类--{{$categoryname}} &nbsp;&nbsp;&nbsp;&nbsp;
									<span class="btn green button-submit" id='addattr'>添加一个商品属性
									<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
									<span class="btn green button-submit" id='delattr'>删除一个商品属性
									<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
								</h3>

							<!-- 错误提示框 -->
							<!-- error -->
	
				             <div class="alert alert-error close" id='error'>
									<strong></strong> 
							 </div>
		

							<!-- /错误提示框 -->
							<div id='cloneattr'>
								<div class="control-group" >
									<label class="control-label">属性名称<span class="required">*</span></label>
									<div class="controls">
										<input type="text" class="span6 m-wrap" name="attr1">                                    
									</div>
								</div>
						   </div>
								<!-- 点击添加的商品属性 -->
								<div id='appendattr'>
									
								</div>
								<!-- /点击添加的商品属性 -->
							</div>

						</div>

						<div class="form-actions clearfix">
				            {{ csrf_field() }}
				            <input type="hidden" name='category_id' value='{{$id}}'>
							<button class='btn blue button-next' id='btn'>点击添加<i class="m-icon-swapright m-icon-white"></i></button>

						</div>

					</div>

				</form>

			</div>

		</div>

	</div>

</div>
<script type="text/javascript">
    //添加一个商品属性
    var i = 1;
	$('#addattr').click(function(){
		i++;
       var cloneattr = $('#cloneattr div:first-child').clone();
       cloneattr.children().eq(1).children().eq(0).attr('name','attr'+i);
       cloneattr.children().eq(1).children().eq(0).val('');
       $('#appendattr').append(cloneattr);
	});
	//删除一个商品属性
	$('#delattr').click(function(){
       $('#appendattr div:last-child').remove();
	});
  $('form').submit(function(){
  	var cloneattrs = $('#cloneattr').find('input');
    if(cloneattrs.val()=='' || $('#appendattr input').val()==''){
    	$('#error').removeClass('close');
    	$('#error strong:first-child').html('属性值不能为空');
      return false;

    }

  });

</script>
@endsection