﻿@extends('layout.index')
@section('content')
<div class="row-fluid">
<div class="span12">

	<!-- BEGIN SAMPLE FORM PORTLET-->   

	<div class="portlet box blue">

		<div class="portlet-title">

			<div class="caption"><i class="icon-reorder"></i>商品管理栏目</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body form">

			<!-- BEGIN FORM-->
<form class="form-horizontal" action="{{url('/admin/product/update')}}" method='post' enctype='multipart/form-data'>

 <div class="tab-content">

<div id="tab1" class="tab-pane active">

	<h3 class="block">修改商品 &nbsp;&nbsp;&nbsp;&nbsp;
	<span class="btn green button-submit" id='addinfo'>修改商品信息
	<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
	<span class="btn green button-submit" id='adddes'>修改商品描述
	<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
	<span class="btn green button-submit" id='addattr'>修改商品属性
	<i class="m-icon-swapright m-icon-white"></i></span>
	</h3>
	<!-- 错误提示框 -->
	<div class="alert alert-error close" id='showerror'>
	<button data-dismiss="alert" class="close"></button>
	<strong></strong> 
    </div>
	<!-- /错误提示框 -->

<!-- 添加商品基本信息 -->
	<div id='p1'>
	<div class="control-group" hb='colneinput'>
		<label class="control-label"><span>商品名称</span><span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="name" class="span6 m-wrap" value="{{$product->name}}">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品描述<span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="describe" class="span6 m-wrap" value="{{$product->describe}}">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品状态<span class="required">*</span></label>
		<div class="controls">
			<label class="radio">
			<div class="radio"><span>
			<input type="radio" value="0"  name="status"  @if($product->status==0)
														  checked
														  @endif
			></span></div>无</label>			
			
			<label class="radio">
			<div class="radio"><span>
			<input type="radio" value="1" name="status" @if($product->status==1)
														 checked
														 @endif
														 ></span></div>热销</label>
			
			<label class="radio">
			<div class="radio"><span class="checked">
			<input type="radio"  value="2" name="status" @if($product->status==2)
														 checked
														 @endif
														 ></span></div>促销</label>  
			
			<label class="radio">
			<div class="radio"><span>
			<input type="radio" value="3" name="status" @if($product->status==3)
														 checked
														 @endif
														 ></span></div>新品</label>  			
		</div>
	</div>
	<div class="control-group" >
		<label class="control-label">商品价格<span class="required">*</span></label>
		<div class="controls">
			<input type="text" id="submit_form_password" name="price" value='{{$product->price}}' class="span6 m-wrap">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">商品库存<span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="stock" value='{{$product->stock}}' class="span6 m-wrap">
			<span class="help-inline">Confirm your password</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">商品栏目<span class="required">*</span></label>
		<div class="controls">
			<select name="category" class="span6 m-wrap">
				@foreach($category as $k=>$v)
				<option value="{{$v->id}}" @if($product->category == $v->id)
                                              selected
                                           @endif
				>
				{{str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',substr_count($v->path,',')-1).'|-'.$v->name}}
				</option>
				@endforeach
			</select>
			<span class="help-inline">Provide your email address</span>
		</div>
	</div>
<div class="control-group">
<div style='span2 m-wrap'></div>
<!-- pic1 -->
	<div data-provides="fileupload" class="fileupload fileupload-new span3 m-wrap">
		<div style="width: 250px; height: 250px; overflow:hidden" class="fileupload-new thumbnail">
			<img alt="{{$product->name}},还没有添加图片!!!" src="{{url($product->pic1)}}" width='250' id='pic1'>
		</div>
		<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
		<div>
			<input type="file" class="default" name='pic1'>
		</div>
	</div>
<!-- /pic1 -->	
<!-- pic2 -->
	<div data-provides="fileupload" class="fileupload fileupload-new span3 m-wrap">
		<div style="width: 250px; height: 250px; overflow:hidden" class="fileupload-new thumbnail">
			<img alt="{{$product->name}},还没有添加图片!!!" src="{{url($product->pic2)}}" width='250' id='pic2'>
		</div>
		<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
		<div>
			<input type="file" class="default" name='pic2'>
		</div>
	</div>
<!-- /pic2 -->
<!-- pic3 -->
	<div data-provides="fileupload" class="fileupload fileupload-new span3 m-wrap">
		<div style="width: 250px; height: 250px; overflow:hidden" class="fileupload-new thumbnail">
			<img alt="{{$product->name}},还没有添加图片!!!" src="{{url($product->pic3)}}" width='250' id='pic3'>
		</div>
		<div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
		<div>
			<input type="file" class="default" name='pic3'>
		</div>
	</div>
<!-- /pic3 -->
</div>

</div>

<!-- 添加商品基本信息 -->
<!-- ue编辑器 -->
<div id="p2" style='display:none'>
	<script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/lang/zh-cn/zh-cn.js"></script>
	<div class="control-group">
	<script id="editor" type="text/plain" name="ue" style="width:100%;height:500px;">{!! $product->ue !!}</script>
	</div>  
	<script>
	var ue = UE.getEditor('editor');
	</script>
</div>
<!-- ue编辑器 -->

<!-- 添加详细属性 -->
<div id='p3' style='display:none'>
  <div class="control-group">
		<label class="control-label">友情提示:<span class="required">*</span></label>
		<div class="controls">
		<select  class="span6 m-wrap" name='categoryattr' id='selectattr'>
		</select>
          <span class="help-inline" style='background:red;color:white;padding:6px;margin-top:-1px'>
		多个商品属性用 | 分割,最多三个属性值</span>			
		</div>
	</div>
	<div id='appendattr'>
		
	</div>
</div>
<!-- /添加详细属性 -->

	<!-- 提交 -->
	{{csrf_field()}}
	<div class="form-actions clearfix">
	<button class="btn green button-submit" id='addpro'>保存修改
	<input type="hidden" name='id' id='productid' value='{{$product->id}}'>
	<i class="m-icon-swapright m-icon-white"></i></button>
	</div>
	<!-- /提交 -->
</div>
</div>
</form>
<!-- END FORM-->    
	</div>
	</div>

	<!-- END SAMPLE FORM PORTLET-->

</div>
</div>
<script type="text/javascript">

  var p1 = $('#p1') ;
  var p2 = $('#p2') ;
  var p3 = $('#p3') ;
   // 显示商品信息
  $('#addinfo').click(function(){
  	 p1.fadeIn();
  	 p3.fadeOut();
     p2.fadeOut();
  });
   // 显示商品描述
  $('#adddes').click(function(){
  	 p1.fadeOut();
  	 p3.fadeOut();
     p2.fadeIn();
  });
  // 显示商品属性
  var status = 0;
  var val = $('#productid').val();
  $('#addattr').click(function(){
  	 p1.fadeOut();
  	 p3.fadeIn();
     p2.fadeOut();
     //请求商品分类
     if(status == 0){
     	$.get('/admin/product/selectattr',{id:val,category:'value'},function(data){
		if(data==0){
			$('#showerror').removeClass('close');
			$('#showerror').find('strong').html('此商品暂无属性,请添加');
			$('#selectattr').show();
			// 加载分类
			$.get('/admin/product/selectattr',{id:0,category:'category'},function(date){
            for(var i in date){
               var opt = $('<option></option>');
               opt.attr('value',date[i].id);
               opt.html(date[i].name);
               $('#selectattr').append(opt);
            }
            //加载分类属性
            $('#selectattr').change(function(){
			var val = $(this).val();
			var appendattr = $('#appendattr');
			$.get('/admin/product/selectattr',{id:val,category:'attr'},function(data){
				appendattr.children().remove();
				if(data==0){
					$('#showerror').removeClass('close');
					$('#showerror').find('strong').html('此商品暂无属性');
				}else{
					$('#showerror').addClass('close');
					for(var i in data){
					var inp = p1.children().eq(0).clone();
					inp.children().eq(0).children().eq(0).html(data[i].name);
					inp.children().eq(1).children().eq(0).attr('name',data[i].name);
					inp.children().eq(1).children().eq(0).val('');
					appendattr.append(inp);
					}				
				}
			})
			});
     	});
		}else{
			$('#selectattr').hide();
			$('#showerror').addClass('close');
			for(var i in data){
			var inp = p1.children().eq(0).clone();
			inp.children().eq(0).children().eq(0).html(data[i]['attr_name']);
			inp.children().eq(1).children().eq(0).attr('name',data[i]['attr_name']);
			inp.children().eq(1).children().eq(0).val(data[i]['attr_value']);
			$('#appendattr').append(inp);
			
		}
            status = 1;
     	}
     });
   }
  });


</script>

@endsection

