﻿@extends('layout.index')
@section('content')

<div class="row-fluid">
<div class="span12">

	<!-- BEGIN EXAMPLE TABLE PORTLET-->

	<div class="portlet box light-grey">

		<div class="portlet-title">

			<div class="caption"><i class="icon-globe"></i>Managed Table</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body">

			<div class="clearfix">

			<div role="grid" class="dataTables_wrapper form-inline" id="sample_1_wrapper">
			<div class="row-fluid">
			<div class="span6"><div class="dataTables_filter" id="sample_1_filter">
			</div></div></div>
			<!-- 错误提示框 -->

			<!-- error -->
			@if(session('error'))
             <div class="alert alert-error">
					<strong>错误提示! {{session('error')}}</strong> 
			 </div>
			 @endif
			 <!-- success -->
			 @if(session('success'))
             <div class="alert alert-success">
					<strong>成功提示! {{session('success')}}</strong> 
			 </div>
			 @endif
			<!-- /错误提示框 -->

			<table id="sample_1" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample_1_info">

				<thead>
					<tr role="row">
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 174px;" aria-label="Username: activate to sort column ascending">排序</th>
					<th class="hidden-480 sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 296px;" aria-label="Email">分类名称</th>
					<th class="hidden-480 sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 172px;" aria-label="Joined">添加子栏目</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 177px;" aria-label="">修改栏目</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 177px;" aria-label="">删除栏目</th>
					</tr>
				</thead>
			<tbody role="alert" aria-live="polite" aria-relevant="all">
			<!-- 分类显示 -->
			@foreach($category as $k)
			<tr class="gradeX odd">
				<td class=" ">{{$k->id}}</td>

				<td class="hidden-480 ">
				<?php
					echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',substr_count($k->path,',')-1).'|--'.$k->name;
				?>
				</td>


				<td class="center hidden-480 "><a href="/admin/category/add?id={{$k->id}}&name={{$k->name}}">添加子分类</a></td>

				<td class=" "><a href="/admin/category/showedit/?id={{$k->id}}&name={{$k->name}}">修改分类</a></td>
				<td class=" "><a href="javascript:void(0)" class='del' data="{{$k->id}}">删除分类</a></td>

			</tr>
			@endforeach
		 <!-- /分类显示 -->
         </tbody></table>
         <div class="row-fluid"><div class="span6"><div class="dataTables_info" id="sample_1_info">Showing 1 to 5 of 25 entries</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← <span class="hidden-480">Prev</span></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#"><span class="hidden-480">Next</span> → </a></li></ul></div></div></div></div>

		</div>

	</div>

	<!-- END EXAMPLE TABLE PORTLET-->

</div>

</div>
<script type="text/javascript">
    //ajax删除分类
	$('.del').click(function(){
		var tar = $(this);
        var id = tar.attr('data');
        $.get('/admin/category/delete',{id:id},function(data){
             if(data == '分类删除成功'){
                tar.parents('tr').remove();
                alert('分类删除成功');
             }else if(data == '此栏目下面有子栏目,不能删除'){
             	alert('此栏目下面有子栏目,不能删除')
             }
        });
	})
</script>
@endsection

