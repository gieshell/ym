@extends('layout.index')
@section('content')
<div class="row-fluid">

	<div class="span12">

		<div class="portlet box blue" id="form_wizard_1">

			<div class="portlet-title">

				<div class="caption">

					<i class="icon-reorder"></i> Form Wizard - <span class="step-title">Step 1 of 4</span>

				</div>

				<div class="tools hidden-phone">

					<a href="javascript:;" class="collapse"></a>

					<a href="#portlet-config" data-toggle="modal" class="config"></a>

					<a href="javascript:;" class="reload"></a>

					<a href="javascript:;" class="remove"></a>

				</div>

			</div>

			<div style="display: block;" class="portlet-body form">


					<div class="form-wizard form-horizontal">

						<div class="navbar steps">

							<div class="navbar-inner">

								<ul class="row-fluid nav nav-pills">

									<li class="span3 active">

										<a href="#tab1" data-toggle="tab" class="step active">

										<span class="number">1</span>

										<span class="desc"><i class="icon-ok"></i> Account Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab2" data-toggle="tab" class="step">

										<span class="number">2</span>

										<span class="desc"><i class="icon-ok"></i> Profile Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab3" data-toggle="tab" class="step">

										<span class="number">3</span>

										<span class="desc"><i class="icon-ok"></i> Billing Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab4" data-toggle="tab" class="step">

										<span class="number">4</span>

										<span class="desc"><i class="icon-ok"></i> Confirm</span>   

										</a> 

									</li>

								</ul>

							</div>

						</div>

						<div id="bar" class="progress progress-success progress-striped">

							<div style="width: 25%;" class="bar"></div>

						</div>

						<div class="tab-content">

							<div class="alert alert-error hide">

								<button class="close" data-dismiss="alert"></button>

								You have some form errors. Please check below.

							</div>

							<div class="alert alert-success hide">

								<button class="close" data-dismiss="alert"></button>

								Your form validation is successful!

							</div>

							<div class="tab-pane active" id="tab1">

								<h3 class="block">修改属性--{{$categoryname}} &nbsp;&nbsp;&nbsp;&nbsp;
								</h3>

							<!-- 错误提示框 -->
							<!-- error -->
	
				             <div class="alert alert-success close" id='success'>
									<strong></strong> 
							 </div>
		

							<!-- /错误提示框 -->

		                       @foreach($attr as $k=>$v)
								<div class="control-group" >
									<label class="control-label">属性名称<span class="required">*</span></label>
									<div class="controls">
										<input type="text" class="span6 m-wrap"   attrid="{{$v->id}}" value="{{$v->name}}">
										<button class='btn blue button-next' hb='attr'>保存修改</button>                                   
									</div>
								</div>
								@endforeach
							</div>

						</div>

						<div class="form-actions clearfix">
				            <input type="hidden" name='category_id' id='category_id' value='{{$id}}'>
							<a href="{{url('/admin/attribute')}}" class='btn blue button-next'>返回属性列表<i class="m-icon-swapleft m-icon-white"></i></a>

						</div>

					</div>

			</div>

		</div>

	</div>

</div>
<script type="text/javascript">
$(function(){
$('input:text').each(function(){
	var original = $(this).val();
	$(this).data('original',original);

});
})
//删除属性
$('button[hb=attr]').click(function(){
	var res = confirm('[[所有的商品属性都将修改]],确认修改!!!');
    if(res){
    	// 属性id
      var attrid = $(this).prev().attr('attrid');
       //属性名称
      var attrname = $(this).prev().val();
       //分类id
      var category_id = $('#category_id').val();
      //原始数据
      var originalattr = $(this).prev().data('original');
       $.get('/admin/attribute/save',{'id':attrid,'attrname':attrname,'category_id':category_id,'originalattr':originalattr},function(data){
           if(data == 1){
           	$('button[hb=attr]').prev().attr('disabled',true);
           	$('#success').removeClass('close');
           	$('#success').find('strong').html('属性修改成功');
           }
      });
    }
})
</script>
@endsection