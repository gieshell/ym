﻿@extends('layout.index')
@section('content')


	<!-- BEGIN EXAMPLE TABLE PORTLET-->

	<div class="portlet box light-grey">

		<div class="portlet-title">

			<div class="caption"><i class="icon-globe"></i>Managed Table</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body">
            
			<div class="clearfix">

			<!-- 错误提示框 -->
             <div class="alert close" id='show'>
					<strong></strong> 
			 </div>
			<!-- error -->
			@if(session('error'))
             <div class="alert alert-error">
					<strong>错误提示! {{session('error')}}</strong> 
			 </div>
			 @endif
			 <!-- success -->
			 @if(session('success'))
             <div class="alert alert-success">
					<strong>成功提示! {{session('success')}}</strong> 
			 </div>
			 @endif
			<!-- /错误提示框 -->
             <div class="row-fluid">
	             <div class="span6" style="margin-top:50px">
					<div id="sample_1_length" class="dataTables_length">
						<form action="/admin/product/index" method='get'>
						<label>
						   <select size="1" name="num" aria-controls="sample_1" class="m-wrap small">

						   		<option value="5" 
								   		@if(!empty($request['num']) && $request['num']==5)
								   		selected
								   		@endif
						   		>5</option>
						   		<option value="10" 
										@if(!empty($request['num']) && $request['num']==10)
								   		selected
								   		@endif
						   		>10</option>
						   		<option value="20" 
	                                   @if(!empty($request['num']) && $request['num']==20)
								   		selected
								   		@endif
						   		>20</option>
						   		<option value="30" 
	                                   @if(!empty($request['num']) && $request['num']==30)
								   		selected
								   		@endif
						   		>30</option>
						   	</select> 自定义显示
						</label>
					</div>
				</div>
				<div class="span6">
				     <div class="dataTables_filter" id="sample_1_filter">
				     	<label>搜索: <input type="text" placeholder="按照商品名称查找"   class="m-wrap medium" name="search" style='height:34px' value="{{$request['search'] or ''}}">
				     	<button class="btn green">查找<i class="m-icon-swapright m-icon-white"></i></button></label>
				    </div>
				</div>
		   				</form>
			</div>
			<table id="sample_1" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample_1_info">

				<thead>
					<tr role="row">
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 174px;" aria-label="Username: activate to sort column ascending">排序</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 296px;" >商品名称</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 172px;" >商品价格</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 172px;" >商品库存</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 172px;" >商品销量</th>
					<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 177px;">修改产品</th>
					<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 177px;" >删除产品</th>
					</tr>
				</thead>
			<tbody role="alert" aria-live="polite" aria-relevant="all">
			<!-- 产品显示 -->
			@foreach($page as $k=>$v)
             <tr class="gradeX odd">
				<td class=" ">{{$v->id}}</td>
				<td class="hidden-480 ">{{$v->name}}</td>
				<td class="hidden-480 ">{{$v->price}}</td>
				<td class="hidden-480 ">{{$v->stock}}</td>
				<td class="hidden-480 ">{{$v->sales}}</td>
				<td class="hidden-480 "><img src="{{trim($v->pic1,'.')}}" alt="" width="85"></td>
				<td class=" " style='width: 300px;'>
				<a href="{{url('/admin/product/showup',['id'=>$v->id])}}"><span class="btn mini green"><i class="icon-plus"></i>修改商品</span></a>
				<span class="btn mini green" hb='{{$v->id}}' ids='del'><i class="icon-plus"></i>删除商品</span>
				</td>
			</tr>
			@endforeach
		    <!-- /产品显示 -->
         </tbody>
         </table>
         <div class="row-fluid">
         <div class="span6">
         <div class="dataTables_info" id="sample_1_info">Showing 1 to 5 of 25 entries</div>
         </div>
         
         <div class="span6">
         <div class="dataTables_paginate paging_bootstrap pagination">
         <ul>
         {!! $page->appends($request)->render() !!}
         </ul>
         </div></div></div></div>

		</div>

	</div>

	<!-- END EXAMPLE TABLE PORTLET-->

<script type="text/javascript">
	$('span[ids=del]').click(function(){
		var id = $(this).attr('hb');
		var span = $(this);
		$.get('/admin/product/del',{id:id},function(data){
             if(data==1){
                 $('#show').removeClass('close');
                 $('#show').addClass('alert-success');
                 $('#show').find('strong').html('友情提示:商品删除成功');
                 span.parents('tr').remove();
             }else{
                 $('#show').removeClass('close');
                 $('#show').addClass('alert-error');
                 $('#show').find('strong').html('友情提示:商品删除失败');
             }
		});
	})

</script>
@endsection

