﻿@extends('layout.index')
@section('content')

<div class="row-fluid">
<div class="span12">

	<!-- BEGIN EXAMPLE TABLE PORTLET-->

	<div class="portlet box light-grey">

		<div class="portlet-title">

			<div class="caption"><i class="icon-globe"></i>商品分类属性管理</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body">

			<div class="clearfix">

			<div role="grid" class="dataTables_wrapper form-inline" id="sample_1_wrapper">
			<div class="row-fluid">
			<div class="span6"><div class="dataTables_filter" id="sample_1_filter">
			</div></div></div>
			<!-- 错误提示框 -->

			<!-- error -->
			@if(session('error'))
             <div class="alert alert-error">
					<strong>错误提示! {{session('error')}}</strong> 
			 </div>
			 @endif
			 <!-- success -->
			 @if(session('success'))
             <div class="alert alert-success">
					<strong>成功提示! {{session('success')}}</strong> 
			 </div>
			 @endif
			<!-- /错误提示框 -->

			<table id="sample_1" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample_1_info">

				<thead>
					<tr role="row">
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 174px;" aria-label="Username: activate to sort column ascending">排序</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 296px;" >分类名称</th>
					<th class="hidden-480 sorting_disabled" rowspan="1" colspan="1" style="width: 172px;" >分类属性操作</th>					
					</tr>
				</thead>
			<tbody role="alert" aria-live="polite" aria-relevant="all">
			<!-- 产品显示 -->
			@foreach($category as $k=>$v)
             <tr class="gradeX odd">
				<td class=" ">{{$v->id}}</td>
				<td class="hidden-480 ">{{$v->name}}</td>
				<td class=" ">
				<a href="{{url('/admin/attribute/edit',['id'=>$v->id,'name'=>$v->name])}}" class="del"><span class="btn mini red"><i class="icon-trash"></i>修改属性</span></a>
				<a href="{{url('/admin/attribute/add',['id'=>$v->id,'name'=>$v->name])}}" class="del"><span class="btn mini red"><i class="icon-trash"></i>添加属性</span></a>
				<a href="{{url('/admin/attribute/showdel',['id'=>$v->id,'name'=>$v->name])}}"><span class="btn mini green"><i class="icon-plus"></i>删除属性</span></a>
				</td>
			</tr>
			@endforeach
		    <!-- /产品显示 -->
         </tbody>
         </table>
         </div>

		</div>

	</div>

	<!-- END EXAMPLE TABLE PORTLET-->

</div>
<div>
@endsection

