﻿@extends('layout.index')
@section('content')
<div class="row-fluid">
<div class="span12">

	<!-- BEGIN SAMPLE FORM PORTLET-->   

	<div class="portlet box blue">

		<div class="portlet-title">

			<div class="caption"><i class="icon-reorder"></i>添加商品</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body form">

			<!-- BEGIN FORM-->
<form class="form-horizontal" action="{{url('/admin/product/insert')}}" method='post' enctype='multipart/form-data'>

 <div class="tab-content">

<div id="tab1" class="tab-pane active">

	<h3 class="block">添加新商品 &nbsp;&nbsp;&nbsp;&nbsp;
	<span class="btn green button-submit" id='addinfo'>添加商品信息
	<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
	<span class="btn green button-submit" id='adddes'>添加商品描述
	<i class="m-icon-swapright m-icon-white"></i></span>&nbsp;&nbsp;
	<span class="btn green button-submit" id='addattr'>添加商品属性
	<i class="m-icon-swapright m-icon-white"></i></span>
	</h3>
	<!-- 错误提示框 -->
	<div class="alert alert-error close" id='showerror'>
	<button data-dismiss="alert" class="close"></button>
	<strong></strong> 
    </div>
	<!-- /错误提示框 -->

<!-- 添加商品基本信息 -->
	<div id='p1'>
	<div class="control-group" hb='colneinput'>
		<label class="control-label"><span>商品名称</span><span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="name" class="span6 m-wrap">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品描述<span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="describe" class="span6 m-wrap">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品状态<span class="required">*</span></label>
		<div class="controls">
			<label class="radio">
			<div class="radio"><span><input type="radio" value="0" checked name="status"></span></div>
			无
			</label>			
			<label class="radio">
			<div class="radio"><span><input type="radio" value="1" name="status"></span></div>
			热销
			</label>
			<label class="radio">
			<div class="radio"><span class="checked"><input type="radio"  value="2" name="status"></span></div>
			促销
			</label>  
			<label class="radio">
			<div class="radio"><span><input type="radio" value="3" name="status"></span></div>
			新品
			</label>  			
		</div>
	</div>
	<div class="control-group" >
		<label class="control-label">商品价格<span class="required">*</span></label>
		<div class="controls">
			<input type="text" id="submit_form_password" name="price" class="span6 m-wrap">
			<span class="help-inline">Provide your username</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">商品库存<span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="stock" class="span6 m-wrap">
			<span class="help-inline">Confirm your password</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">商品栏目<span class="required">*</span></label>
		<div class="controls">
			<select name="category" class="span6 m-wrap">
				@foreach($category as $k=>$v)
				<option value="{{$v->id}}">
				{{str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',substr_count($v->path,',')-1).'|-'.$v->name}}
				</option>
				@endforeach
			</select>
			<span class="help-inline">Provide your email address</span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品图片1<span class="required">*</span></label>
		<div class="controls">
			<input type="file" name="pic1" class="default">
			<span class="help-inline">Confirm your password</span>
		</div>
	</div>	
	<div class="control-group">
		<label class="control-label">商品图片2<span class="required">*</span></label>
		<div class="controls">
			<input type="file" name="pic2" class="default">
			<span class="help-inline">Confirm your password</span>
		</div>
	</div>	
	<div class="control-group">
		<label class="control-label">商品图片3<span class="required">*</span></label>
		<div class="controls">
			<input type="file" name="pic3" class="default">
			<span class="help-inline">Confirm your password</span>
		</div>
	</div>	

</div>
<!-- 添加商品基本信息 -->
<!-- ue编辑器 -->
<div id="p2" style='display:none'>
	<script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/lang/zh-cn/zh-cn.js"></script>
	<div class="control-group">
	<script id="editor" type="text/plain" name="ue" style="width:100%;height:500px;">商品描述</script>
	</div>  
	<script>
	var ue = UE.getEditor('editor');
	</script>
</div>
<!-- ue编辑器 -->

<!-- 添加详细属性 -->
<div id='p3' style='display:none'>
  <div class="control-group">
		<label class="control-label">选择分类<span class="required">*</span></label>
		<div class="controls">
			<select  class="span6 m-wrap" name='categoryattr' id='selectattr'>
			</select>		
			<span class="help-inline" style='background:red;color:white;padding:6px;margin-top:-1px'>
		多个商品属性用 | 分割,最多三个属性值</span>
			
		</div>
	</div>
	<div id='appendattr'>
		
	</div>
</div>
<!-- /添加详细属性 -->

	<!-- 提交 -->
	{{csrf_field()}}
	<div class="form-actions clearfix">
	<button class="btn green button-submit" id='addpro'>点击添加
	<i class="m-icon-swapright m-icon-white"></i></button>
	</div>
	<!-- /提交 -->
</div>
</div>
</form>
<!-- END FORM-->    
	</div>
	</div>

	<!-- END SAMPLE FORM PORTLET-->

</div>
</div>
<script type="text/javascript">

  var p1 = $('#p1') ;
  var p2 = $('#p2') ;
  var p3 = $('#p3') ;
   // 显示商品信息
  $('#addinfo').click(function(){
  	 p1.fadeIn();
  	 p3.fadeOut();
     p2.fadeOut();
  });
   // 显示商品描述
  $('#adddes').click(function(){
  	 p1.fadeOut();
  	 p3.fadeOut();
     p2.fadeIn();
  });
  // 显示商品属性
  var status = 0;
  $('#addattr').click(function(){
  	 p1.fadeOut();
  	 p3.fadeIn();
     p2.fadeOut();
     //请求商品分类
     if(status == 0){
     	$.get('/admin/product/selectattr',{id:0,category:'category'},function(date){
            for(var i in date){
               var opt = $('<option></option>');
               opt.attr('value',date[i].id);
               opt.html(date[i].name);
               $('#selectattr').append(opt);
            }
            status = 1;
     	});
     }

  });
//请求商品分类属性
$('#selectattr').change(function(){
	var val = $(this).val();
	var appendattr = $('#appendattr');
	$.get('/admin/product/selectattr',{id:val,category:'attr'},function(data){
		appendattr.children().remove();
		if(data==0){
			$('#showerror').removeClass('close');
			$('#showerror').find('strong').html('此商品暂无属性');
		}else{
			$('#showerror').addClass('close');
			for(var i in data){
			var inp = p1.children().eq(0).clone();
			inp.children().eq(0).children().eq(0).html(data[i].name);
			inp.children().eq(1).children().eq(0).attr('name',data[i].name);
			inp.children().eq(1).children().eq(0).val('');
			appendattr.append(inp);
			}
			
		}

	})
});
</script>

@endsection

