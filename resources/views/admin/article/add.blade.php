@extends('layout.index')
@section('title', '文章添加页')
@section('content')
<div class="row-fluid">
<div class="span12">

	<!-- BEGIN SAMPLE FORM PORTLET-->   

	<div class="portlet box blue">

		<div class="portlet-title">

			<div class="caption"><i class="icon-reorder"></i>添加文章</div>

			<div class="tools">

				<a class="collapse" href="javascript:;"></a>

				<a class="config" data-toggle="modal" href="#portlet-config"></a>

				<a class="reload" href="javascript:;"></a>

				<a class="remove" href="javascript:;"></a>

			</div>

		</div>

		<div class="portlet-body form">

			<!-- BEGIN FORM-->
<form class="form-horizontal" action="{{url('/admin/article/insert')}}" method='post'>

 <div class="tab-content">

<div id="tab1" class="tab-pane active">

	<h3 class="block">添加文章 &nbsp;&nbsp;&nbsp;&nbsp;

	</h3>
	<!-- 错误提示框 -->
	<div class="alert alert-error close" id='showerror'>
	<button data-dismiss="alert" class="close"></button>
	<strong></strong> 
    </div>
	<!-- /错误提示框 -->

<!-- 添加商品基本信息 -->
	<div class="control-group">
		<label class="control-label"><span>标题</span><span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="ar_name" class="span6 m-wrap">
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">作者<span class="required">*</span></label>
		<div class="controls">
			<input type="text" name="ar_author" class="span6 m-wrap">
			<span class="help-inline"></span>
		</div>
	</div>

	<div class="control-group">

		<label class="control-label">描述<span class="required">*</span></label>

		<div class="controls">

			<textarea rows="3" class="span6 m-wrap" name="ar_des"></textarea>

		</div>

	</div>


<!-- 添加商品基本信息 -->
<!-- ue编辑器 -->
	<script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/adminPublic/edit/lang/zh-cn/zh-cn.js"></script>
	<div class="control-group">
	<script id="editor" type="text/plain" name="ar_content" style="width:100%;height:500px;"></script>
	</div>  
	<script>
	var ue = UE.getEditor('editor');
	</script>

<!-- ue编辑器 -->


	<!-- 提交 -->
	{{csrf_field()}}
	<div class="form-actions clearfix">
	<button class="btn green button-submit" id='addpro'>点击发布
	<i class="m-icon-swapright m-icon-white"></i></button>
	</div>
	<!-- /提交 -->
</div>
</div>
</form>
<!-- END FORM-->    
	</div>
	</div>

	<!-- END SAMPLE FORM PORTLET-->

</div>
</div>
@endsection

