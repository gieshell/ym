@extends('layout.index')
@section('title', '后台文章列表')
@section('content')
<div class="portlet box light-grey">

	<div class="portlet-title">

		<div class="caption"><i class="icon-globe"></i>文章信息表</div>

		<div class="tools">

			<a href="javascript:;" class="collapse"></a>

			<a href="#portlet-config" data-toggle="modal" class="config"></a>

			<a href="javascript:;" class="reload"></a>

			<a href="javascript:;" class="remove"></a>

		</div>

	</div>

	<div class="portlet-body">

		<div class="clearfix">

			<div class="btn-group">
				<form action="/admin/article/add">
					<button id="sample_editable_1_new" class="btn green">

					添加文章 <i class="icon-plus"></i>

					</button>
				</form>
			</div>
			<!-- success代表成功 -->
			@if(session('success'))
			<div class="alert alert-success">
				
				<button class="close" data-dismiss="alert"></button>

				<strong>{{session('success')}}</strong>

			</div>
			@endif

				<ul class="dropdown-menu pull-right">

					<li><a href="#">Print</a></li>

					<li><a href="#">Save as PDF</a></li>

					<li><a href="#">Export to Excel</a></li>

				</ul>

			</div>

		</div>

		<div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid">
			<div class="row-fluid"><div class="span6">
				<div id="sample_1_length" class="dataTables_length">
				<form action="/admin/article/index">
					<label>
					   <select size="1" name="num" aria-controls="sample_1" class="m-wrap small">
					   		<option value="5" @if(!empty($request['num']) && $request['num'] == 5)
											 	selected
											  @endif
							>5</option>
					   		<option value="10" @if(!empty($request['num']) && $request['num'] == 10)
											 	selected
											   @endif>10</option>
					   		<option value="20"@if(!empty($request['num']) && $request['num'] == 20)
											 	selected
											  @endif>20</option>
					   		<option value="30"@if(!empty($request['num']) && $request['num'] == 30)
											 	selected
											  @endif>30</option>
					   	</select> 自定义显示</label></div></div><div class="span6">
					     <div class="dataTables_filter" id="sample_1_filter">
					     	<label>搜索: <input type="text" placeholder="按照文章名查找" value="{{$request['keywords'] or ''}}"  aria-controls="sample_1" class="m-wrap medium" name="keywords"><button class="btn green" >查找<i class="m-icon-swapright m-icon-white"></i></button></label>
					     </div>
				</div>
				</form>
		</div>
		<table class="table table-striped table-bordered table-hover dataTable" id="sample_1" aria-describedby="sample_1_info">

			<thead>

				<tr role="row">
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 136.199999988079px;">ID</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 136.199999988079px;">文章题目</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 136.199999988079px;">文章作者</th>
					<th class="hidden-480 sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="Joined" style="width: 201.199999988079px;">文章描述</th>
					<th class="hidden-480 sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="Joined" style="width: 201.199999988079px;">添加时间</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 196.199999988079px;">文章操作</th>
				</tr>

			</thead>

			
		
		<tbody role="alert" aria-live="polite" aria-relevant="all">
		@foreach($info as $k=>$v)
		<tr class="gradeX odd">

					<td class=" ">{{$v->id}}</td>

					<td class="hidden-480 ">{{$v->ar_name}}</td>

					<td class="hidden-480">{{$v->ar_author}}</td>
					<td class="hidden-480 ">{{substr($v->ar_des,0,18)}}</td>

					<td class="center hidden-480 "> {{date('Y-m-d H:i:s',$v->ar_time)}}</td>

					<td class=" ">
						<a href="{{url('/admin/article/edit',[$v->id])}}"><span class="btn mini green"><i class="icon-plus"></i>修改</span></a>
						<a href="{{url('/admin/article/delete',[$v->id])}}" ><span class="btn mini red"><i class="icon-trash"></i>删除</span></a>
					</td>

		</tr>	
		@endforeach
		</tbody>
		</table>
			<div class="row-fluid">
					<div  style="float:right" class=" dataTables_paginate paging_bootstrap pagination">
					      {!! $info->appends($request)->render() !!}
					</div>
			</div>
		</div>

	</div>

</div>
<script>
	// //绑定事件
	// $('.del').click(function(){
	// 	var res = confirm('您确定要删除吗');
	// 	if(!res){ return false;}
	// 	//获取id 发送ajax
	// 	var id = $(this).attr('a');
	// 	var t = $(this);
	// 	$.get("{{url('/admin/article/delete')}}",{id:id},function(data){
	// 		if(data == 2){
	// 			alert('删除失败');
	// 		}else{
	// 			//删除元素 在ajax中$(this).指向了ajax对象
	// 			t.parents('tr').remove();
	// 		}
	// 	})
	// })
</script>
@endsection