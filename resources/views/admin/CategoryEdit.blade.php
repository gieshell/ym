@extends('layout.index')
@section('content')
<div class="row-fluid">

	<div class="span12">

		<div class="portlet box blue" id="form_wizard_1">

			<div class="portlet-title">

				<div class="caption">

					<i class="icon-reorder"></i> Form Wizard - <span class="step-title">Step 1 of 4</span>

				</div>

				<div class="tools hidden-phone">

					<a href="javascript:;" class="collapse"></a>

					<a href="#portlet-config" data-toggle="modal" class="config"></a>

					<a href="javascript:;" class="reload"></a>

					<a href="javascript:;" class="remove"></a>

				</div>

			</div>

			<div style="display: block;" class="portlet-body form">

				<form novalidate="novalidate" action="/admin/category/edit" class="form-horizontal" id="submit_form" method='post'>

					<div class="form-wizard">

						<div class="navbar steps">

							<div class="navbar-inner">

								<ul class="row-fluid nav nav-pills">

									<li class="span3 active">

										<a href="#tab1" data-toggle="tab" class="step active">

										<span class="number">1</span>

										<span class="desc"><i class="icon-ok"></i> Account Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab2" data-toggle="tab" class="step">

										<span class="number">2</span>

										<span class="desc"><i class="icon-ok"></i> Profile Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab3" data-toggle="tab" class="step">

										<span class="number">3</span>

										<span class="desc"><i class="icon-ok"></i> Billing Setup</span>   

										</a>

									</li>

									<li class="span3">

										<a href="#tab4" data-toggle="tab" class="step">

										<span class="number">4</span>

										<span class="desc"><i class="icon-ok"></i> Confirm</span>   

										</a> 

									</li>

								</ul>

							</div>

						</div>

						<div id="bar" class="progress progress-success progress-striped">

							<div style="width: 25%;" class="bar"></div>

						</div>

						<div class="tab-content">

							<div class="alert alert-error hide">

								<button class="close" data-dismiss="alert"></button>

								You have some form errors. Please check below.

							</div>

							<div class="alert alert-success hide">

								<button class="close" data-dismiss="alert"></button>

								Your form validation is successful!

							</div>

							<div class="tab-pane active" id="tab1">

								<h3 class="block">修改分类--{{$category['name']}}</h3>

								<div class="control-group">

									<label class="control-label">分类名称<span class="required">*</span></label>

									<div class="controls">

										<input type="text" value="{{$category['name']}}" class="span6 m-wrap" name="categoryname">
                                        <input type="hidden" name='id' value="{{ $category['id'] }}">
                                         {{ csrf_field() }}
									</div>

								</div>

							</div>

						</div>

						<div class="form-actions clearfix">
				
							<button class='btn blue button-next'>点击修改<i class="m-icon-swapright m-icon-white"></i></button> 

						</div>

					</div>

				</form>

			</div>

		</div>

	</div>

</div>
@endsection