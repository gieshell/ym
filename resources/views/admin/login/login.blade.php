<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>驭梦商城会员登录</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />

	<!-- BEGIN GLOBAL MANDATORY STYLES -->

	<link href="/adminPublic/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style-metro.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

	<link href="/adminPublic/media/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>

	<link href="/adminPublic/media/css/uniform.default.css" rel="stylesheet" type="text/css"/>

	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->

	<link href="/adminPublic/media/css/login-soft.css" rel="stylesheet" type="text/css"/>

	<!-- END PAGE LEVEL STYLES -->

	<link rel="shortcut icon" href="/adminPublic/media/image/favicon.ico" />

</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="login">

	<!-- BEGIN LOGO -->

	<div class="logo">

		<img src="/adminPublic/media/image/logo-big.png" alt="" /> 

	</div>

	<!-- END LOGO -->

	<!-- BEGIN LOGIN -->

	<div class="content">

		<!-- BEGIN LOGIN FORM -->

		<form class="form-vertical login-form" action="/admin/login/check" method="post">
				{{ csrf_field() }}
				<h3 class="form-title">驭梦商城会员登录</h3>
			@if(session('error'))
				<div class="alert alert-error">

					<button class="close" data-dismiss="alert"></button>

					<span>{{session('error')}}</span>

				</div>
			@endif
			      	@if (count($errors) > 0)
				        <div class="alert alert-error">
				            <ul>
				                @foreach ($errors->all() as $error)
				                    <li>{{ $error }}</li>
				                @endforeach
				            </ul>
				        </div>
			   		 @endif

			<div class="alert alert-error hide">

				<button class="close" data-dismiss="alert"></button>

				<span>用户名密码不能为空</span>

			</div>

			<div class="control-group">

				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->

				<label class="control-label visible-ie8 visible-ie9">用户名</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-user"></i>

						<input class="m-wrap placeholder-no-fix" type="text" placeholder="用户名" name="username"/>

					</div>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">密码</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-lock"></i>

						<input class="m-wrap placeholder-no-fix" type="password" placeholder="密码" name="password"/>

					</div>

				</div>

			</div>

			<div class="forget-password">           
				<input type="submit" value="登录" class="btn green big btn-block">
			</div>

			<div class="create-account">

				<p>
					<center>
					没有帐号?点击此 
					
					<a href="javascript:;" id="register-btn" class="">注册</a>
					</center>
				</p>

			</div>

		</form>

		<!-- END LOGIN FORM -->        


		<!-- BEGIN REGISTRATION FORM -->

		<form class="form-vertical register-form" action="/admin/login/insert" method="post">
				{{ csrf_field() }}
				<h3 class="">用户注册</h3>
			@if(session('error'))
				<div class="alert alert-error">

					<button class="close" data-dismiss="alert"></button>

					<span>{{session('error')}}</span>

				</div>
			@endif
	      	@if (count($errors) > 0)
		        <div class="alert alert-error">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
	   		 @endif
			

			<p>输入您的帐户详细信息如下:</p>

			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">用户名</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-user"></i>

						<input class="m-wrap placeholder-no-fix" type="text" placeholder="用户名" name="username"/>

					</div>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">密码</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-lock"></i>

						<input class="m-wrap placeholder-no-fix" type="password" id="register_password" placeholder="密码" name="password"/>

					</div>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">确认密码</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-ok"></i>

						<input class="m-wrap placeholder-no-fix" type="password" placeholder="确认密码" name="repassword"/>

					</div>

				</div>

			</div>

			<div class="control-group">


				<label class="control-label visible-ie8 visible-ie9">邮箱</label>

				<div class="controls">

					<div class="input-icon left">

						<i class="icon-envelope"></i>

						<input class="m-wrap placeholder-no-fix" type="text" placeholder="用于激活帐号使用" name="email"/>

					</div>

				</div>

			</div>
			<!-- 验证密码开始-->
			<div class="control-group">

				<label class="control-label visible-ie8 visible-ie9">验证码</label>

				<div class="controls">

					<div class="input-icon left">

						<i class=" icon-eye-open"></i>
			
						<input type="text" name="vcode" class="m-wrap small" placeholder="验证码">
						<img src="{{url('/admin/login/vcode')}}" style="width:88px"onclick="this.src=this.src+'?a=1'" alt="">
					</div>
					
				</div>

			</div>
			<!-- 验证密码结束 -->



			<div class="control-group">


			</div>

			<div class="form-actions">

				<button id="register-back-btn" type="button" class="btn">

				<i class="m-icon-swapleft"></i>  返回

				</button>

				<button type="submit" id="register-submit-btn" class="btn blue pull-right">

				注册 <i class="m-icon-swapright m-icon-white"></i>

				</button>            

			</div>

		</form>

		<!-- END REGISTRATION FORM -->

	</div>

	<!-- END LOGIN -->

	<!-- BEGIN COPYRIGHT -->

	<div class="copyright">

		2016 &copy; 驭梦IT公司

	</div>

	<!-- END COPYRIGHT -->

	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

	<!-- BEGIN CORE PLUGINS -->

	<script src="/adminPublic/media/js/jquery-1.10.1.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

	<script src="/adminPublic/media/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      

	<script src="/adminPublic/media/js/bootstrap.min.js" type="text/javascript"></script>

	<!--[if lt IE 9]>

	<script src="/adminPublic/media/js/excanvas.min.js"></script>

	<script src="/adminPublic/media/js/respond.min.js"></script>  

	<![endif]-->   

	<script src="/adminPublic/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.blockui.min.js" type="text/javascript"></script>  

	<script src="/adminPublic/media/js/jquery.cookie.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.uniform.min.js" type="text/javascript" ></script>

	<!-- END CORE PLUGINS -->

	<!-- BEGIN PAGE LEVEL PLUGINS -->

	<script src="/adminPublic/media/js/jquery.validate.min.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/jquery.backstretch.min.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->

	<!-- BEGIN PAGE LEVEL SCRIPTS -->

	<script src="/adminPublic/media/js/app.js" type="text/javascript"></script>

	<script src="/adminPublic/media/js/login-soft.js" type="text/javascript"></script>      

	<!-- END PAGE LEVEL SCRIPTS --> 

	<script>

		jQuery(document).ready(function() {     

		  App.init();

		  Login.init();

		});

	</script>
	<script>
		$('form').submit(function(){
			//触发所有的丧失焦点事件
			$('input').trigger('blur');
			if(USER){
				return true;
			}

			//阻止默认行为
			return false;
		})

		//用户名丧失焦点事件
		$('input[name=username]').blur(function(){
			//检测用户名是否正确
			var reg = /^\w{5,18}$/;
			//获取用户名
			var username = $(this).val();
			if(reg.test(username)){
				$(this).next().children().attr('class','icon-ok');
				$(this).next().attr('class','input-success tooltips');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group success');
			}else{
				//错误
				$(this).next().children().attr('class','icon-exclamation-sign');
				$(this).next().attr('class','input-error error');
				$(this).parent('div').addClass('controls input-icon');
				$(this).parents('.control-group').attr('class','control-group error');
				return false;
			}
			var inp = $(this);
			//get发送
			$.get('/admin/login/select',{username:username},function(data){
				if(data == 2){
					inp.next().children().attr('class','icon-ok');
					inp.next().attr('class','input-success tooltips');
					inp.parent('div').addClass('controls input-icon');
					inp.parents('.control-group').attr('class','control-group success');
					$('#useryz').attr('class','alert alert-block alert-success fade in').children().html('可以使用该用户名');
					$('#jdt').css('width','25%');
					$('.jd').addClass('active');
					//修改全局变量
					USER = true;
				}else{
					inp.next().children().attr('class','icon-exclamation-sign');
					inp.next().attr('class','input-error error');
					inp.parent('div').addClass('controls input-icon');
					inp.parents('.control-group').attr('class','control-group error');
					$('#useryz').attr('class','alert alert-block alert-error fade in').children().html('该用户名已经存在');

					return false;
				}
			//第四个参数 如果要加参数,返回的格式就必须是json格式
			},'json')
		})

	</script>

	<!-- END JAVASCRIPTS -->

</body>

<!-- END BODY -->

</html>