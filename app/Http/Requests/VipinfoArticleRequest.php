<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VipinfoArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  判断是否满足验证规则。
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/^\d{11}$/',
            'email' => 'required|email',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [

            'phone.required'  => '手机号是必填的',
            'phone.regex'  => '手机号不满足格式',
            'email.required'  => '邮箱是必填的',
            'email.email'  => '邮箱格式不正确',
        ];
    }
}
