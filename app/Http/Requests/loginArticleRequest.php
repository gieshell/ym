<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class loginArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  判断是否满足验证规则。
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
            'repassword' => 'required|same:password',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => '用户名是必填的',
            'password.required'  => '密码是必填的',
            'repassword.required'  => '确认密码是必填的',
            'repassword.same'  => '两次密码必须相同的',
        ];
    }
}
