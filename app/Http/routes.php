<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('404',function(){
	abort(404);
});

Route::group(['middleware'=>'login'],function(){
	//后台首页
	Route::Controller('/admin/','AdminController');
	//后台用户
	Route::Controller('/admin/user','UserController');
	//退出登录
	Route::get('/admin/login/loginout','LoginController@loginout');
	// ------------------------------------------ 洪波
	Route::controller('/admin/category','CategoryController');
	Route::controller('/admin/product','ProductController');
	Route::controller('/admin/attribute','AttributeController');
	// ------------------------------------------ /洪波
	// ------------------------------------------ 刘丹
	//文章管理
	Route::controller('admin/article','ArticleController');
	//订单管理页
	Route::controller('admin/order','OrderController');

	// ------------------------------------------ /刘丹
});


//后台登录
Route::Controller('/admin/login','LoginController');

//前台个人用户首页
Route::Controller('/user/','VipController');
//这个是后台用户的修改路游
Route::Controller('/user/info','VipinfoController');




