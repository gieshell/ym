<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\loginArticleRequest;
use App\Http\Requests\InsertArticleRequest;
//引用对应的命名空间
use Gregwar\Captcha\CaptchaBuilder;
use Session;
class LoginController extends Controller
{
    /**
     * 这个是后台登录
     * /admin/login/index
     */
    public function getIndex()
    {
    	return view('admin.login.login');
    }



    /**
     * 这个是登录处理页面(前后台)
     * /admin/login/Check
     * /admin/home/login
     */
    public function postCheck(InsertArticleRequest $request)
    {
    	//查询用户是否存在
    	$user = DB::table('ym_user')->where('username',$request->input('username'))->first();
        //判断 用户名是否存在
        if($user){
            //存在的时候并且登录 admin
                 if($user->username == 'admin'){
                    if(Hash::check($request->input('password'),$user->password)){
                        //登录成功 用户id放入session中
                        session(['id'=>$user->id,'login'=>'1']);
                        return redirect('/admin')->with('success','欢迎您'.$user->username.'登录')->with('username',$user->username);
                    }else{
                        return back()->with('error','用户名或密码错误');
                    }
                }else{
            //不是admin的时候跳转前台
                    if(Hash::check($request->input('password'),$user->password)){
                        //登录成功 用户id放入session中
                        session(['id'=>$user->id]);
                        return redirect('/user/info');
                    }else{
                        return back()->with('error','用户名或密码错误');
                    }
                }
        }else{
             return back()->with('error','用户名或密码错误');
                
        }

    }

    /**
     * 这个是给前台注册用户的ajax使用的
     * /admin/user/select
     */
    public function getSelect()
    {
        $date = $_GET['username'];
        $username = DB::select('select * from ym_user where username="'.$date.'"');
        if($username){
            //1 是用户名存在
            echo 1;
        }else{
            //2 是用户名不存在
            echo 2;
        }

    }


    /**
     * 这个是后台用户退出
     * /admin/loginout
     */

    public function loginout(Request $request)
    {
        //用户退出登录
        $request->session()->flush();
        return redirect('admin/login');
    }

    /**
     * 这个是前台用户注册
     * /home/register
     */
    public function postInsert(loginArticleRequest $request){
       //检测验证码是否正确
       if($request->input('vcode') != session('Vcode')){
            return  back()->with('error','验证码错误');
       } 
        $date = $request->except(['repassword','_token']);
        //加密密码
        $date['password'] = Hash::make($request->input('password'));
        //对token字段进行处理随机生成50个字符串
        $date['token'] = str_random(50);
        $date['regtime'] = time();
        //将数据插入数据库中
        $res = DB::table('ym_user')->insert($date);
        if($res){
            return redirect('admin/user/index')->with('success','用户添加成功');
        }else{
            return redirect('admin/user/add');
        }
    }


    /**
     * 这个是验证码
     */

    public function getVcode()
    {   
        ob_clean();//清除缓存区
        //生成验证码图片的Builder对象，配置相应属性
        $builder = new CaptchaBuilder;
        //可以设置图片宽高及字体
        $builder->build($width = 100, $height = 40, $font = null);
        //获取验证码的内容
        $phrase = $builder->getPhrase();

        //把内容存入session
        session(['Vcode'=>$phrase]);
        //生成图片
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: image/jpeg');
        $builder->output();
    }
}
