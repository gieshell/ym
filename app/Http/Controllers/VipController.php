<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VipController extends Controller
{
    /**
     * 这个是前台用户中线
     * /user/index
     */
    public function getIndex()
    {
    	return view('user.index');
    }
    
}

