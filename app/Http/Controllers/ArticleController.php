<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    //文章添加
    public function getAdd()
    {
    	return view('admin.article.add');
    }
    
    //数据的插入
    public function postInsert(Request $request)
    {
    	$data=$request->except(['_token']);

    	$data['ar_time']=time();

    	$res=DB::table('ym_article')->insert($data);

    	if($res){
    		return redirect('/admin/article/index')->with('success','添加文章成功');
    	}else{
    		return back()->with('error','添加文章失败');
    	}

    	// dd($data);

    }


    //文章列表

    public function getIndex(Request $request)
    {
    	// return view('article.index');
		//读取信息  分页
		//判断是否有搜索信息
		if(empty($request->input('keywords'))){
			$ml=DB::table('ym_article')->orderBy('ar_time','desc')->paginate($request->input('num',5));
		} else{

			$ml=DB::table('ym_article')->orderBy('ar_time','desc')->where('ar_name','like','%'.$request->input('keywords').'%')->paginate($request->input('num',5));
		}   	
    	// dd($data);


    	//解析模板
    	return view('admin.article.index',['info'=>$ml,'request'=>$request->all()]);



    }

    //文章修改

    public function getEdit($id)
    {   
        $res=DB::table('ym_article')->where('id',$id)->first();

    	return view('admin.article.edit',['info'=>$res]);
    }

    //修改数据
    public function postUpdate(Request $request)
    {
        $data=$request->except(['_token']);
        $data['ar_time']=time();
        $id=$request->input('id');

        $res=DB::table('ym_article')->where('id',$id)->update($data);

        if($res){
            return redirect('/admin/article/index')->with('success','修改文章成功');
        }else{
            return back()->with('error','修改文章失败');
        }
        
    }


    //文章删除

    public function getDelete($id)
    {
    	$res=DB::table('ym_article')->where('id',$id)->delete();

        if($res){
            return redirect('/admin/article/index')->with('success','删除文章成功');
        }else{
            return back()->with('error','删除文章失败');
        }
    }


    
}
