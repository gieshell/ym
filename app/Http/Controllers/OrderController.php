<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    //文章订单列表页
    public function getIndex()
    {
    	return view('admin.order.index');
    }
}
