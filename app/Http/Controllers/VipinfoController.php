<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VipinfoArticleRequest;
class VipinfoController extends Controller
{
    /**
     * 这个是前台用户中心个人信息页面
     * /user/
     */
    public function getIndex(Request $request)
    {	
    	session(['id'=>3]);
    	$id = $request->session()->all();

		$data = DB::table('ym_user')->where('id',$id['id'])->first();
		//计算一下当前用户注册多长事件
		$date =  date('Y-m-d',$data->regtime);
		$newtime=abs((strtotime(date("Y-m-d",time()))-strtotime($date))/86400);
		return view('/user/info/index',['data'=>$data,'newtime'=>$newtime]);
    }

    /**
     * 这个是前台用户中心个人修改信息 (跟个人信息页面一个页面)
     * /user/info/index
     */

    public function postUpdate(VipinfoArticleRequest $request)
    {
    	$date = $request->except(['_token']);
    	$res = DB::table('ym_user')->where('id',$date['id'])->update($date);
    	if($res >= 0){
    		return redirect('/user/info/index')->with('success','用户信息修改成功');
    	}else{
    		return redirect('/user/info/index');
    	}
    }

   /**
    * 这个是前台用户中心个人无刷新上传头像
    * /user/info/imgupload
    */
       public function postImgupload(Request $request)
       {
           $id = $request['id'];
           //获取随机的文件名
           $name = md5(time()+rand(0,19999999));
           //获取上传文件的后缀名
           $houzuiming = $request->file('file')->getClientOriginalExtension();
           //将指定文件移动到指定目录下,参数1 指定的目录 参数2 文件名
           $request->file('file')->move(\Config::get('app.uplodedir'),$name.'.'.$houzuiming);
           $path = trim(\Config::get('app.uplodedir').$name.'.'.$houzuiming,'.');
           $res = DB::table('ym_user')->where('id',$id)->update(['pic'=>$path]);
           echo json_encode($path);

       }
    
}

