<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    //显示商品页面
    public function getIndex(Request $request)
    {
        $page = DB::table('ym_product')->where(function($query)use($request){
           $query->where('name','like','%'.$request->input('search').'%');
        })->paginate($request->input('num',10));
        // $page = DB::table('ym_product')->simplePaginate(5);
        return view('/admin/Product',['page'=>$page,'request'=>$request->all()]);
    }

    //显示添加商品页面
    public function getAdd()
    {   
        //查询商品分类
        $res = $this->selectcategory();
        return view('/admin/ProductAdd',['category'=>$res]);
    }

    //添加商品
    public function postInsert(Request $request)
    {
       // 开启事物处理机制
       DB::beginTransaction();
       // 插入商品信息
       $product = $request->only(["name","describe","status","price","stock","category","ue"]);
       //处理文件上传
        foreach($request->file() as $k=>$v){
            $fileName = md5(time().rand(1,100)).'.'.$request->file($k)->getClientOriginalExtension();
            $request->file($k)->move(\Config::get('app.uploaddir'),$fileName);
            $product[$k] = \Config::get('app.uploaddir').$fileName;
        }
       $product_id = DB::table('ym_product')->insertGetId($product);

       // 插入属性
       $product_attr = $request->except(["categoryattr","name","describe","status","price","stock","category","ue","_token",'pic1','pic2','pic3']);
       $res = 1;
       foreach($product_attr as $k=>$v){
         $arr['attr_name'] = $k;
         $arr['attr_value'] = $v;
         $arr['product_id'] = $product_id;
         $arr['category_id'] = $request->input('categoryattr');
         $res = DB::table('ym_attribute_val')->insert($arr);
       }
       //商品插入成功就提交
      if($res){
        DB::commit();
        return redirect('/admin/product/index')->with('success','商品添加成功');
      }else{
        DB::rollBack();
      }
     
    }
    


    //ajax查询属性
    public function getSelectattr(Request $request)
    {

        //接收id
        $id = $request->input('id');
        if($request->input('category')=='category'){
            //查询分类表
            $category = DB::table('ym_category')->where('pid',$id)->get();
            if($category){
                return $category;
            }else{
                return 0;
            }
        }else if($request->input('category')=='attr'){
          //查询属性标表
            $res = DB::table('ym_product_attr')->where('category_id',$id)->get();
            if($res){
                return $res;
            }else{
                return 0;
            } 
        }else{
          //查询属性值
            $res = DB::table('ym_attribute_val')->select('attr_name','attr_value')->where('product_id',$id)->get();
            if($res){
                return $res;
            }else{
                return 0;
            }  
        }

    }

    //修改产品页面
    public function getShowup($id)
    {
        //根据产品id,查询产品
        $product = DB::table('ym_product')->where('id',$id)->first();
        //获得商品分类
        $category = $this->selectcategory();
        return view('/admin/ProductEdit',['product'=>$product,'category'=>$category]);
        
    }
    //保存修改产品
    public function postUpdate(Request $request)
    {
       //产品id
        // dd($request->all());
       $id = $request->input('id');
       DB::beginTransaction();
       //接收商品信息
       $product = $request->only(["name","describe","status","price","stock","category","ue"]);
       //处理文件上传
       foreach($request->file() as $k=>$v){
        $fileName = md5(time()).rand(1,100).'.'.$request->file($k)->getClientOriginalExtension();
        $request->file($k)->move(\Config::get('app.uploaddir'),$fileName);
        $product[$k] = \Config::get('app.uploaddir').$fileName;
       }
       //更新商品信息
       DB::table('ym_product')->where('id',$id)->update($product);
       //获取商品属性
      $product_attr = $request->except(["categoryattr","name","describe","status","price","stock","category","ue","_token",'pic1','pic2','pic3']);
       //信息更新成功&& 属性不为空
       $res = 1;
       if(!empty($product_attr)){
             //删除此商品的属性,在插入
             $res2 = DB::table('ym_attribute_val')->where('product_id',$id)->delete();
             foreach($product_attr as $k=>$v){
             $arr['attr_name'] = $k;
             $arr['attr_value'] = $v;
             $arr['product_id'] = $id;
             $arr['category_id'] = $request->input('categoryattr');
             $res = DB::table('ym_attribute_val')->insert($arr);
            }
       }
       
       if($res){
          DB::commit();
          return redirect('/admin/product/')->with('success','商品信息修改成功');
       }else{
         DB::rollBack();
         return back()->with('error','商品信息修改失败');
       }
        
    }

    //删除商品
    public function getDel(Request $request)
    { 
        //商品id
        $id = $request->input('id');
        //删除商品
        $res = DB::table('ym_product')->where('id',$id)->delete();
        //删除商品属性
        DB::table('ym_attribute_val')->where('product_id',$id)->delete();
        if($res){
          echo 1;
        }else{
          echo 0;
        }
    }


    //查询分类表
    public function selectcategory()
    {
       return DB::select("select *,concat(path,id) as pathid from ym_category order by pathid");
    }

    
}
